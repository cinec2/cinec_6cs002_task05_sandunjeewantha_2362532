import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FootballLeague02 {
	public static void main(String [] args) {
		List <FootballClub> table = new ArrayList<>();
		
		try (BufferedReader buffered = new BufferedReader(new FileReader("input.csv"))) {
		    String line;
		    while ((line = buffered.readLine()) != null) {
		        String[] value = line.split(",");
		        table.add(new FootballClub(Integer.parseInt(value[0]),value[1],Integer.parseInt(value[2]),Integer.parseInt(value[3]),Integer.parseInt(value[4]),Integer.parseInt(value[5]),Integer.parseInt(value[6]),Integer.parseInt(value[7]), Double.parseDouble(value[8])));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("   Team Name                   won      drawn     lost     points\n");
		table.stream().forEach(x -> System.out.println(x));
	    System.out.println();
	    System.out.println("\n Parallel Stream \n");
	    System.out.println("   Team Name                   won      drawn     lost     points\n");
	    table.parallelStream().forEach(System.out::println);
	
		try {
			FileWriter writer = new FileWriter("Output2.txt");
			writer.write("   Team Name                   won      drawn     lost     points\n");
			writer.write("   ---------                   ---      ------    ----     ------\n");
			table.stream().forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			});
			
			writer.write("\n Parallel Stream \n");
			writer.write("   Team Name                   won      drawn     lost     points\n");
			writer.write("   ---------                   ---      ------    ----     ------\n");
			table.parallelStream().forEach(x -> {
				try {
					writer.write(x.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			
			writer.close();
			System.out.println("\nSuccessfully wrote to the file Output2.txt.");
	    } catch (IOException e) {
	    	System.out.println("An error occurred.");
	    	e.printStackTrace();
	    }
	}
}
