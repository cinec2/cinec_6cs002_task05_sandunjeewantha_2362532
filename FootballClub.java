public class FootballClub implements Comparable<FootballClub>{

	private int number;
	private String teamName;
	private int played;
	private int won;
	private int drawn;
	private int lost;
	private int penalty;
	private int points;
	private double nrr;
	
	public FootballClub(int number, String teamName, int played, int won, int lost, int drawn, int penalty, int points, double nrr) {
	    this.number = number;
	    this.played = played;
	    this.won = won;
	    this.drawn = drawn;
	    this.lost = lost;
	    this.penalty = penalty;
	    this.points = points;
	    this.nrr = nrr;
	    this.teamName = teamName;
	  }
	
	public String toString() {
		return String.format("%-3d%-20s%10d%10d%10d%10d", number, teamName, won, drawn, lost, points);
	}	

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getTeam() {
		return teamName;
	}

  public void setTeamName(String teamName) {
    this.teamName = teamName;
  }

  public int getPlayed() {
    return played;
  }

  public void setPlayed(int played) {
    this.played = played;
  }

  public int getWon() {
    return won;
  }

  public void setWon(int won) {
    this.won = won;
  }

  public int getDrawn() {
    return drawn;
  }

  public void setDrawn(int drawn) {
    this.drawn = drawn;
  }
  

  public int getLost() {
    return lost;
  }

  public void setLost(int lost) {
    this.lost = lost;
  }
  
  public int getPenalty() {
	  return penalty;
  }
  
  public void setPenalty(int penalty) {
	  this.penalty = penalty;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }
  
  public double getNrr() {
    return nrr;
  }

  public void setNrr(double nrr) {
    this.nrr = nrr;
  }
	
	@Override
	public int compareTo(FootballClub clubs) {
		return ((Integer) won).compareTo(clubs.won);
	}
}
