   Team Name                   won      drawn     lost     points
   ---------                   ---      ------    ----     ------
1  Tuskers                      5         2         5         5
2  Beats                        6         0         6         6
3  Heroes                       4         4         4         4
4  Sharks                       3         3         6         4
5  Lords                        3         1         8         3
6  Rhinos                       3         2         7         5
7  Devils                       6         2         4         4
8  Eagles                       9         2         1        10
9  Angles                       2         1         9         7
10 Geniuses                    10         1         1        13

 Parallel Stream 
   Team Name                   won      drawn     lost     points
   ---------                   ---      ------    ----     ------
2  Beats                        6         0         6         6
6  Rhinos                       3         2         7         5
3  Heroes                       4         4         4         4
8  Eagles                       9         2         1        10
5  Lords                        3         1         8         3
1  Tuskers                      5         2         5         5
9  Angles                       2         1         9         7
7  Devils                       6         2         4         4
4  Sharks                       3         3         6         4
10 Geniuses                    10         1         1        13
