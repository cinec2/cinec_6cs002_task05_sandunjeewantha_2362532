import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class FootballLeague03 {
	public static void main(String [] args) {
		List <FootballClub> table = new ArrayList<>();
		
		try (BufferedReader buffered = new BufferedReader(new FileReader("input.csv"))) {
		    String line;
		    while ((line = buffered.readLine()) != null) {
		        String[] value = line.split(",");
		        table.add(new FootballClub(Integer.parseInt(value[0]),value[1],Integer.parseInt(value[2]),Integer.parseInt(value[3]),Integer.parseInt(value[4]),Integer.parseInt(value[5]),Integer.parseInt(value[6]),Integer.parseInt(value[7]), Double.parseDouble(value[8])));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
//		Min function & Map function & OptionalInt
		OptionalInt max = table.stream().mapToInt(FootballClub::getPoints).max();
	    if (max.isPresent()) {
	      System.out.printf("\nHighest number of points is %d\n", max.getAsInt());
	    } else {
	      System.out.println("max failed");
	    }
	    
//	    Reduce function
	    System.out.print("Number of matches drawn: ");
	    Integer result = table.stream().map(FootballClub::getDrawn).reduce(0, (a, b) -> a + b);
	    System.out.println(result);
	    
//	    Collect function
	    List<String> str = table.stream()
	    	    .filter(p -> p.getLost() < 5)
	    	    .map(FootballClub::getTeam)
	    	    .collect(Collectors.toList());
	    	                   
	    	System.out.println("Teams with more than 5 wins: " + str.toString());
	    	
		    
//		Write same results to file
    	try {
		      FileWriter writer = new FileWriter("Output3.txt");
		      writer.write("Highest number of points is " + max.getAsInt() + "\n");
		      writer.write("Number of matches drawn: " + result + "\n");
		      writer.write("Teams with more than 5 wins: " + str + "\n");
		      writer.close();
		      System.out.println("\nSuccessfully wrote to the file Output3.txt.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
}
