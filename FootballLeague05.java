import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FootballLeague05 {
	public static void main(String [] args) {
		List <FootballClub> table = new ArrayList<>();
		
		try (BufferedReader buffered = new BufferedReader(new FileReader("input.csv"))) {
		    String line;
		    while ((line = buffered.readLine()) != null) {
		        String[] value = line.split(",");
		        table.add(new FootballClub(Integer.parseInt(value[0]),value[1],Integer.parseInt(value[2]),Integer.parseInt(value[3]),Integer.parseInt(value[4]),Integer.parseInt(value[5]),Integer.parseInt(value[6]),Integer.parseInt(value[7]), Double.parseDouble(value[8])));
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		Sorted function
		System.out.println("\nSorted by compare wining in FootballClub class\n");
		System.out.println("   Team Name                   won      drawn     lost     points\n");
	    table.stream().sorted().forEach(System.out::println);

	    System.out.println();
	    System.out.println("Sorted by lambda\n");
	    System.out.println("   Team Name                   won      drawn     lost     points\n");
	    table.stream()
	         .sorted((c1, c2) -> 
	            ((Double) c1.getNrr()).compareTo(c2.getNrr()))
	         .forEach(System.out::println);
	    
	    
//	    Write same results to file
	    try {
		      FileWriter writer = new FileWriter("Output5.txt");
		      writer.write("Sorted by compare wining in FootballClub class\n\n");
		      writer.write("   Team Name                   won      drawn     lost     points\n");
		      writer.write("   ---------                   ---      ------    ----     ------\n");
		      table.stream().sorted()
		        .forEach(str -> {
		        	try {
		        		writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      
		      writer.write("\nSorted by lambda\n\n");
		      writer.write("   Team Name                   won      drawn     lost     points\n");
		      writer.write("   ---------                   ---      ------    ----     ------\n");
		      table.stream().sorted((c1, c2) -> 
	            ((Double) c1.getNrr()).compareTo(c2.getNrr()))
		        .forEach(str -> {
		        	try {
		        		writer.write(str.toString() + "\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
		        });
		      writer.close();
		      System.out.println("\nSuccessfully wrote to the file Output5.txt.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
}
